import java.io.*;
import java.util.*;

public class Main {
  public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    Scanner sc = new Scanner(System.in);
    String in = sc.nextLine();
    String[] inc = new String[3];
    inc = in.split(" ");
    int[] returned = new int[3];
    for (int i=0; i<3; i++){
      returned[i] = Integer.parseInt(inc[i]);
      //System.out.println(returned[i]);
    }

    String due1 = sc.nextLine();
    String[] due2 = new String[3];
    due2 = due1.split(" ");
    int[] due = new int[3];
    for (int i=0; i<3; i++){
      due[i] = Integer.parseInt(due2[i]);
      //System.out.println(due[i]);
    }

    int fine = 0;

    if (returned[0] == due[0] && returned[2] == due[2]){
      if (due[1] < returned[1]){
        fine = (returned[1] - due[1])*15;
      }
    }

    if (returned[0] > due[0] && returned[2] == due[2]){
      fine = fine = (returned[0] - due[0])*500;
    } 

    if (returned[2] > due[2]){
      fine = fine = (returned[2] - due[2])*10000;
    }          

    System.out.println(fine);

    }
    }


